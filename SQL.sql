﻿DROP DATABASE IF EXISTS mariposas;
CREATE DATABASE IF NOT EXISTS mariposas;
USE mariposas;
CREATE TABLE ejemplar (
  nejemplar int,
  procedencia varchar (40),
  tamaño varchar (40),
  codcol int,
  PRIMARY KEY (nejemplar, codcol),
  CONSTRAINT colejemplar FOREIGN KEY (codcol) REFERENCES coleccion (codcol)
);
CREATE TABLE mariposa(
ncient varchar (50),
  origen varchar (40),
  habitat varchar (40),
  espvida int,
  color varchar (40),
  nejemplar int,
  codcol int,
  PRIMARY KEY (ncient, nejemplar, nespecie),
  CONSTRAINT mariejem FOREIGN KEY (nejemplar) REFERENCES ejemplar (nejemplar),
  CONSTRAINT mariespecie FOREIGN KEY (nespecie) REFERENCES especie (nespecie)
);
CREATE TABLE especie( 
  nespecie int,
  caracteristicas varchar (40),
  PRIMARY KEY (nespecie),
);
CREATE TABLE coleccion (
codcol int,
  precio int,
  ubicacion varchar (40),
  PRIMARY KEY (codcol),
);
CREATE TABLE persona (
  dni int,
  npersona int,
  direccion varchar (40),
  tfno int,
  codcol int,
  esprincipal varchar,
  PRIMARY KEY (dni, codcol),
  CONSTRAINT percol FOREIGN KEY (codcol) REFERENCES coleccion (codcol)
  );
CREATE TABLE mejorejem (
nespecie int,
  codcol int,
  mejorejemplar varchar (50),
  PRIMARY KEY (nespecie, codcol),
  CONSTRAINT mejespec FOREIGN KEY (nespecie) REFERENCES especie (nespecie),
  CONSTRAINT mejcol FOREIGN KEY (codcol) REFERENCES coleccion (codcol)
);
